"""AshhServer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
from pathlib import Path

from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from AshhServer import settings

assets = settings.BASE_DIR / 'static/assets'
print(assets)
urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("main.urls")),
    re_path(r'assets/(?P<path>.*)$', serve, {'document_root': assets}),
    re_path(r'ngsw-worker.js/$', serve, {'document_root': settings.BASE_DIR / 'static'}),
    re_path(r'/manifest.*$', serve, {'document_root': settings.BASE_DIR / 'static'}),
    # path('direction.svg', serve, {'document_root': os.path.join(settings.BASE_DIR, '/static/assets')})
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
