from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class User (AbstractUser):
    wallet = models.IntegerField(null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    email = models.EmailField(unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Singles (models.Model):
    name = models.CharField(max_length=400, null=True, blank=True)
    cover = models.ImageField(upload_to="cover/singels", null=True, blank=True)
    duration = models.CharField(max_length=400, null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    lyric = models.CharField(max_length=400, null=True, blank=True)
    Feat = models.CharField(max_length=400, null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    Co_produced = models.CharField(max_length=400, null=True, blank=True)
    MixMaster = models.CharField(max_length=400, null=True, blank=True)
    Artwork = models.CharField(max_length=400, null=True, blank=True)
    cover_creator = models.CharField(max_length=400, null=True, blank=True)
    Producer = models.CharField(max_length=400, null=True, blank=True)
    labels = models.CharField(max_length=400, null=True, blank=True)
    youTube = models.CharField(max_length=400, null=True, blank=True)
    baseColor = models.CharField(max_length=400, null=True, blank=True)
    spotify = models.CharField(max_length=400, null=True, blank=True)
    soundcloud = models.CharField(max_length=400, null=True, blank=True)
    CssClass = models.CharField(max_length=400, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    main = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name}"


class Albums (models.Model):
    name = models.CharField(max_length=400, null=True, blank=True)
    cover = models.ImageField(upload_to="cover/albums", null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    Producer = models.CharField(max_length=400, null=True, blank=True)
    labels = models.CharField(max_length=400, null=True, blank=True)
    youTube = models.CharField(max_length=400, null=True, blank=True)
    spotify = models.CharField(max_length=400, null=True, blank=True)
    cover_creator = models.CharField(max_length=400, null=True, blank=True)
    phone_cover = models.ImageField(upload_to="cover/albums", null=True, blank=True)
    soundcloud = models.CharField(max_length=400, null=True, blank=True)
    CssClass = models.CharField(max_length=400, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    main = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name}"


class AlbumMusic (models.Model):
    name = models.CharField(max_length=400, null=True, blank=True)
    Album = models.ForeignKey(Albums, related_name='AlbumMusic', on_delete=models.CASCADE)
    Producer = models.CharField(max_length=400, null=True, blank=True)
    labels = models.CharField(max_length=400, null=True, blank=True)
    duration = models.CharField(max_length=400, null=True, blank=True)
    Feat = models.CharField(max_length=400, null=True, blank=True)
    youTube = models.CharField(max_length=400, null=True, blank=True)
    spotify = models.CharField(max_length=400, null=True, blank=True)
    soundcloud = models.CharField(max_length=400, null=True, blank=True)
    music = models.FileField(upload_to="music/albums", null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class AlbumSells (models.Model):
    user = models.ForeignKey(User, related_name="buy", on_delete=models.CASCADE)
    product = models.ForeignKey(Albums, related_name="MusicBuy", null=True, blank=True, on_delete=models.CASCADE)
    payment = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)


class MusicSells (models.Model):
    user = models.ForeignKey(User, related_name="MusicBuy", on_delete=models.CASCADE)
    product = models.ForeignKey(Singles, related_name="MusicBuy", null=True, blank=True, on_delete=models.CASCADE)
    payment = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)


class Product(models.Model):
    name = models.CharField(max_length=400, null=True, blank=True)
    pic = models.ImageField(upload_to="cover/albums", null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    code = models.CharField(max_length=400, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class ProductSells (models.Model):
    user = models.ForeignKey(User, related_name="ProductBuy", on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name="ProductBuy", null=True, blank=True, on_delete=models.CASCADE)
    address = models.CharField(max_length=400, null=True, blank=True)
    payment = models.CharField(max_length=100, null=True, blank=True)
    postCode = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)


class Events (models.Model):
    name = models.CharField(max_length=400, null=True, blank=True)
    discription = models.TextField(null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"