from django.contrib import admin

# Register your models here.
from main.models import User, MusicSells, AlbumMusic, Albums, AlbumSells, Singles, ProductSells, Events, Product

admin.site.register(User)
admin.site.register(MusicSells)
admin.site.register(AlbumMusic)
admin.site.register(Albums)
admin.site.register(AlbumSells)
admin.site.register(Singles)
admin.site.register(ProductSells)
admin.site.register(Events)
admin.site.register(Product)
