import logging

from django.contrib.auth import get_user_model
from djoser.serializers import UserSerializer
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response

from main.models import User, Albums, Singles, AlbumMusic

logger = logging.getLogger('django')


class Login(APIView):

    def post(self, request):

        if request.data['username'] and request.data['password']:
            if "@" in request.data['username']:
                kwargs = {'email': request.data['username']}
            else:
                kwargs = {'username': request.data['username']}
            try:
                user = get_user_model().objects.get(**kwargs)
                if user.check_password(request.data['password']):
                    logger.info("here")
                    token, created = Token.objects.get_or_create(user=user)
                    return Response({
                        'token': token.key,
                        'user': {
                            'id': user.id,
                            'email': user.email,
                            'username': user.username,
                            'firstname': user.first_name,
                            'lastname': user.last_name,
                            'wallet': user.wallet,
                            'group': user.groups.first().name,
                            'phone': user.phone,
                            'address': user.address,
                        }
                    })
                else:
                    data = dict()
                    logger.exception("login faild! password not correct")
                    data['error'] = "نام کاربری یا رمز عبور اشتباه است!"
                    return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data=data)
            except Exception as e:
                logger.exception("login faild! User not found")
                data = dict()
                data['error'] = "نام کاربری یا رمز عبور اشتباه است!"
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data=data)
        else:
            data = dict()
            data['error'] = "نام کاربری یا رمز عبور اشتباه است!"
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data=data)


class MainApi(APIView):
    def get(self, request):
        data = dict()
        if Albums.objects.filter(main=True).exists():
            album = Albums.objects.get(main=True)
            data = {
                "id": album.id,
                "name": album.name,
                "cover": album.cover.url,
                "main": album.main,
                "album": True,
                "single": False,
            }
        elif Singles.objects.filter(main=True).exists():
            single = Singles.objects.get(main=True)
            data = {
                "id": single.id,
                "name": single.name,
                "cover": single.cover.url,
                "main": single.main,
                "album": False,
                "single": True,
            }
        print(data)
        return Response(data)


class MusicsApi(APIView):
    def get(self, request):
        albumsList = list()
        singlesList = list()
        if Albums.objects.all().exists():
            albums = Albums.objects.all().order_by("-date")
            for album in albums:
                albumsList.append({
                    "id": album.id,
                    "name": album.name,
                    "cover": album.cover.url,
                    "price": album.price,
                    "Producer": album.Producer,
                    "labels": album.labels,
                    "youTube": album.youTube,
                    "spotify": album.spotify,
                    "cover_creator": album.cover_creator,
                    "phoneCover": album.phone_cover.url,
                    "soundcloud": album.soundcloud,
                    "CssClass": album.CssClass,
                    "date": album.date,
                    "main": album.main,
                    "type": "album",
                })
        if Singles.objects.all().exists():
            singles = Singles.objects.all().order_by("-date")
            for single in singles:
                singlesList.append({
                    "id": single.id,
                    "name": single.name,
                    "cover": single.cover.url,
                    "duration": single.duration,
                    "price": single.price,
                    "lyric": single.lyric,
                    "Producer": single.Producer,
                    "Feat": single.Feat,
                    "text": single.text,
                    # "phoneCover": single.phone_cover.url,
                    "Co_produced": single.Co_produced,
                    "MixMaster": single.MixMaster,
                    "Artwork": single.Artwork,
                    "cover_creator": single.cover_creator,
                    "labels": single.labels.split(","),
                    "youTube": single.youTube,
                    "baseColor": single.baseColor,
                    "spotify": single.spotify,
                    "soundcloud": single.soundcloud,
                    "CssClass": single.CssClass,
                    "main": single.main,
                    "date": single.date,
                    "type": "single",
                    "SpecialTnx": "lilage wear"
                })

        return Response({"albums": albumsList, "singles": singlesList})


class AlbumsApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)


class SinglesApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)


class AlbumApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)

    def post(self, request):
        data = list()
        print(request.data)
        albumeMusic = AlbumMusic.objects.filter(Album_id=request.data["id"])
        for music in albumeMusic:
            data.append({
                "name": music.name,
                "duration": music.duration,
                "id": music.duration,
                "price": music.price
            })
        return Response(data)


class SingleApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)

    def post(self, request):
        data = dict()
        return Response(data)


class ShopApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)

    def post(self, request):
        data = dict()
        return Response(data)


class EventApi(APIView):
    def get(self, request):
        data = dict()
        return Response(data)
